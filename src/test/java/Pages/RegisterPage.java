package Pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uimaps.RegisterMap;

public class RegisterPage {

    WebDriver driver;
    RegisterMap registerMap = new RegisterMap();

    public RegisterPage (WebDriver driver){
        this.driver = driver;
    }

    public void navegar(String url){
        driver.navigate().to(url);
    }

    public void escreverNome(String nome){
        WebElement inputNome = driver.findElement(registerMap.inputNome);
        inputNome.sendKeys("Leandro");
    }

    public void escreverCompany(String company){
        WebElement inputCompany = driver.findElement(registerMap.inputCompany);
        inputCompany.sendKeys("Everis");
    }

    public void escreverEmail(String email){
        WebElement inputEmail = driver.findElement(registerMap.inputEmail);
        inputEmail.sendKeys("leandro@everis.com");
    }

    public void escreverPassword(String password){
        WebElement inputPassord = driver.findElement(registerMap.inputPassord);
        inputPassord.sendKeys("1234567");
    }

    public void escreverConfirmPassword(String ConfirmPassword){
        WebElement inputConfirmPassword = driver.findElement(registerMap.ConfirmPassword);
        inputConfirmPassword.sendKeys("1234567");
    }

    public void clickRegister(){
        WebElement clickRegister = driver.findElement(registerMap.clickRegister);
        clickRegister.click();
    }

    public void confereTextoregister() throws InterruptedException {
        String ActualText = driver.findElement(registerMap.ActualText).getText();
        Assert.assertEquals(ActualText,"You are logged in!");
        Thread.sleep(1000);
    }

    public void clickBlazerDemo(){
        WebElement clickBlazerDemo = driver.findElement(registerMap.clickBlazerdemo);
        clickBlazerDemo.click();
    }

}
