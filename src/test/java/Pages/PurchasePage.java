package Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import uimaps.PurchaseMap;

public class PurchasePage {

    WebDriver driver;
    PurchaseMap purchaseMap = new PurchaseMap();


    public PurchasePage (WebDriver driver){
        this.driver = driver;
    }

        public void navegar(String url){
            driver.navigate().to(url);
        }

    public void escreverNome(String nome){
        WebElement inputNome = driver.findElement(purchaseMap.inputNome);
        inputNome.sendKeys("Leonardo");
    }

    public void escreveEndereco(String endereco){
        WebElement inputEndereço = driver.findElement(purchaseMap.inputEndereco);
        inputEndereço.sendKeys("Rua Antonio Dias");
    }

    public void escreveCidade(String cidade){
        WebElement inputCidade = driver.findElement(purchaseMap.inputCidade);
        inputCidade.sendKeys("Uberlândia");
    }

    public void escreveEstado(String Estado){
        WebElement inputEstado = driver.findElement(purchaseMap.inputEstado);
        inputEstado.sendKeys("Minas Gerais");
    }

    public void escreveZipCode(String zipCode){
        WebElement inputZipCode = driver.findElement(purchaseMap.inputZipCode);
        inputZipCode.sendKeys("30408126");
    }

    /*
    public void SelecionaTipoCartao(String tipoCartao){
        WebElement inputTipoCartao = driver.findElement(purchaseMap.inputTipoCartao);
        Select selecionaCartao = new Select(SelecionaTipoCartao(tipoCartao);
        selecionaCartao.selectByVisibleText("teste");
    }
     */


    public void escreveNumeroCartao(String numeroCartao){
        WebElement inputNumeroCartao = driver.findElement(purchaseMap.inputNumeroCartaoCredito);
        inputNumeroCartao.sendKeys("88888222888");
    }

    public void escreveMes(String mes){
        WebElement inputMes = driver.findElement(purchaseMap.inputMes);
        inputMes.clear();
        inputMes.sendKeys("10");
    }

    public void escreveYear(String ano){
        WebElement inputAno = driver.findElement(purchaseMap.inputAno);
        inputAno.clear();
        inputAno.sendKeys("2020");
    }

    public void escreveNomeCartao(String nomeCartao){
        WebElement inputNomeNoCartao = driver.findElement(purchaseMap.inputNomeNoCartao);
        inputNomeNoCartao.sendKeys("Leonardo C Lemes");
    }

    public void clicar(){
        WebElement clicPurchaseFlight = driver.findElement(purchaseMap.clicPurchaseFlight);
        clicPurchaseFlight.click();
    }

    public void confereTexto(){
        String ActualText = driver.findElement(purchaseMap.ActualText).getText();
        Assert.assertEquals(ActualText,"Thank you for your purchase today!");
    }
}
