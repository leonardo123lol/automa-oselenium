package scenarios;

import Pages.PurchasePage;
import Pages.RegisterPage;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class CT01 {

    WebDriver driver;
    RegisterPage registerPage;
    String ActualText;


    @BeforeClass
    public static void beforeClass(){
        System.setProperty("webdriver.chrome.driver","C:\\Users\\Leonardo\\Drivers\\chromedriver.exe");
    }


    @Before
    public void before(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        registerPage = new RegisterPage(driver);
    }


    @Test
    public void RunTest() throws InterruptedException {
        registerPage.navegar("http://blazedemo.com/register");
        registerPage.escreverNome("");
        registerPage.escreverCompany("");
        registerPage.escreverEmail("");
        registerPage.escreverPassword("");
        registerPage.escreverConfirmPassword("");
        registerPage.clickRegister();
        registerPage.confereTextoregister();
        registerPage.clickBlazerDemo();

    }

}
