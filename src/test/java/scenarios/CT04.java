package scenarios;

import Pages.PurchasePage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class CT04 {

    WebDriver driver;
    PurchasePage purchasePage;
    String ActualText;

    @BeforeClass
    public static void beforeClass(){
        System.setProperty("webdriver.chrome.driver","C:\\Users\\Leonardo\\Drivers\\chromedriver.exe");
    }

    @Before
    public void before(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        purchasePage = new PurchasePage(driver);
    }

    @Test
    public void runTest(){
        purchasePage.navegar("http://blazedemo.com/purchase.php");
        purchasePage.escreverNome("");
        purchasePage.escreveEndereco("");
        purchasePage.escreveCidade("");
        purchasePage.escreveEstado("");
        purchasePage.escreveZipCode("");
        purchasePage.escreveNumeroCartao("");
        purchasePage.escreveMes("");
        purchasePage.escreveYear("");
        purchasePage.escreveNomeCartao("");
        purchasePage.clicar();
        purchasePage.confereTexto();
    }
}
