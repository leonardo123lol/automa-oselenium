package uimaps;

import org.openqa.selenium.By;

public class PurchaseMap {
    public By inputNome = By.id("inputName");
    public By inputEndereco = By.id("address");
    public By inputCidade = By.id("city");
    public By inputEstado = By.id("state");
    public By inputZipCode = By.id("zipCode");
    public By inputTipoCartao = By.name("cardType");
    public By inputNumeroCartaoCredito = By.id("creditCardNumber");
    public By inputMes = By.id("creditCardMonth");
    public By inputAno = By.id("creditCardYear");
    public By inputNomeNoCartao = By.id("nameOnCard");
    public By clicPurchaseFlight = By.xpath("//input[@value='Purchase Flight']");
    public By ActualText = By.xpath("//div[@class='container hero-unit']/h1");




}
