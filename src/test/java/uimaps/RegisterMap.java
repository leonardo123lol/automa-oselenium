package uimaps;

import org.openqa.selenium.By;

public class RegisterMap {

    public By inputNome = By.id("name");
    public By inputCompany = By.id("company");
    public By inputEmail = By.id("email");
    public By inputPassord = By.id("password");
    public By ConfirmPassword = By.id("password-confirm");
    public By clickRegister = By.xpath("//div[@class='col-md-6 col-md-offset-4']/button");
    public By ActualText = By.xpath("//div[@class='panel-body']/text()");
    public By clickBlazerdemo = By.xpath("//*[@id=\"app\"]/nav/div/div[1]/a");

}
